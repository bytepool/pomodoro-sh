INST_LOC=/usr/bin
INST_LOC_LOCAL=~/bin
PROG=pomodoro-sh
VER=0.2
DESC="A simple bash script for using the pomodoro time management technique."

all:
	@echo "Nothing to compile. To install pomodoro-sh, type 'make install' as root."

install: $(PROG) man
	mkdir -p $(INST_LOC)
	cp $(PROG) $(INST_LOC)
	chmod +x $(INST_LOC)/$(PROG)

install-local: $(PROG) 
	mkdir -p $(INST_LOC_LOCAL)
	cp $(PROG) $(INST_LOC_LOCAL)
	chmod +x $(INST_LOC_LOCAL)/$(PROG)

man:
	help2man --section 1 -N --name=$(DESC) --source="$(PROG) $(VER)" --include=AUTHORS -o $(PROG).1 ./$(PROG)

.PHONY: install man
